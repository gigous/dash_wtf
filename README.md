# DashForm

Package to build WTForms-style forms in Plotly Dash.

Install dependencies:

```bash
$ python3.7 -m venv env
# activate environment
$ . env/bin/activate
(env) $ pip install -r requirements/requirements.txt
```

See sample_app.py for example.

## Bootstrap

DashForm also supports [dash bootstrap components](https://dash-bootstrap-components.opensource.faculty.ai/). You must install dash bootstrap components before using.

### Change Default To Use Bootstrap Styles

You can change the DashForm default behavior to use bootstrap styles:

```python
from dash_wtf import DashForm
DashForm.DEFAULT_TO_BOOTSTRAP = True

# Define your form
class MyForm(DashForm):
    # your fields
```

### Set Bootstrap Via Parameter

You can also set this for each individual DashForm:

```python
from dash_wtf import DashForm

# Define your form
class MyForm(DashForm):
    # your fields

# create your form
my_form = MyForm(bootstrap=True)
```

### To Run Tests

#### Linux

[Download Chrome driver](http://chromedriver.chromium.org/downloads)

Find your chrome version, then download for OS. Zip contains a binary file. Put the binary somewhere reasonable (like /usr/bin), then add to `$PATH`.

Then, in root folder, run:

```bash
(env) $ python -m pytest tests/
```
