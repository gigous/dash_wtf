"""Default sublayouts used by Dashform to create forms out of the box"""
import dash_html_components as html
try:
    import dash_bootstrap_components as dbc
except ImportError:
    dbc = None
from typing import Union
from dash.development.base_component import Component


Sublayout = Union[callable, Component]


class DashCoreSublayout:
    """Default sublayout to use when DashForm._bootstrap == False
    """
    def __call__(self, component: Component, label: str) -> Sublayout:
        label_comp = [html.Label(label, className='dashform-label')] if label else []
        return html.Div(
            label_comp + [component],
            className='dashform-div'
        )


class DashBootstrapSublayout:
    """Default sublayout to use when DashForm._bootstrap == True
    """
    def __init__(self):
        if not dbc:
            raise Exception("Install 'dash_bootstrap_components' for Bootstrap support.")

    def __call__(self, component: Component, label: str) -> Sublayout:
        label_comp = [dbc.Label(label, className='dashform-label')] if label else []
        return dbc.FormGroup(
            children=[
                dbc.Col(
                    label_comp,
                    className='dashform-label-col',
                    width=3
                ),
                dbc.Col(
                    component,
                    className='dashform-input-col',
                    width=3,
                ),
            ],
            className='dashform-formgroup',
            row=True,
        )
