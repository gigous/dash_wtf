from typing import NoReturn
from dash.development.base_component import Component
from . import exceptions
from ._sublayouts import Sublayout


def _validate_sublayout(layout: Sublayout, component: Component):
    """Adapted from dash.dash._validate._validate_layout (v1.6.0)"""
    if not isinstance(layout, (Component, callable)):
        raise dash.exceptions.NoLayoutException(
            """
            A valid sublayout was not provided to DashForm.
            Must provide a dash component or a function that returns
            a dash component, and contains the provided component.
            """
        )

    # TODO does it handle a callable?
    for layout_component in layout._traverse():
        if layout_component is component:
            return
        # Dash will handle any duplicate components, so don't handle here

    raise exceptions.ComponentNotInSublayoutError(
        """
        Could not find DashForm component in sublayout. The component
        must be added to the dash sublayout in order to function properly.
        """
    )

def _validate_sublayout_loader_defined(sublayout_loader: callable):
    if not sublayout_loader:
        raise exceptions.SublayoutLoaderError(
                """
                Sublayout loader not defined.
                Sublayout loader must be defined as a callable that returns
                a Dash Component, or a callable that returns or a Dash Component.
                """
            )
