"""Dash form builder"""

import warnings
import dash
import dash_core_components as dcc
import dash_html_components as html
from collections import OrderedDict
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from dash.development.base_component import Component
from wtforms.fields.core import Field
try:
    import dash_bootstrap_components as dbc
except ImportError:
    dbc = None

from . import _validate
from . import exceptions
from . import _sublayouts
from ._sublayouts import Sublayout

from wtforms.form import Form
from wtforms import (
    StringField,
    IntegerField,
    BooleanField,
    DateField,
    DateTimeField,
    DecimalField,
    SelectField
)
from typing import Tuple, Any, List, Dict


# TODO: Maybe consider using BaseForm instead?
# TODO: (low priority) Figure out how to change order of fields if inheriting
# TODO: Add ability for disable ability for fields based on other fields
# TODO: Abstract away '<component_id>.value' and such to prevent
#       programmer from having to type that out; values of validated data
#       can/should be easier to access that that
#       (In other words, encapsulate this better)
class BaseDashForm(Form):
    """Base class of DashForm, where all base logic and form validation
    is done. Adapts from wtform.form.Form to a dash layout"""
    def __init__(
        self,
        formdata=None,
        obj=None,
        prefix="",
        data=None,
        meta=None,
        bootstrap: bool=False,
        **kwargs
    ):
        """Base class for building forms in Dash using the WTForms style of
        form construction. This class acts as an adapter between the two.
        When using the forms, once the user puts in their desired input
        and clicks
        submit, the form is validated using the WTForms validation
        framework.

        :type Form: the WTForms class Form, used for declarative definition of fields
        """
        super().__init__(formdata, obj, prefix, data, meta, **kwargs)
        self.post_validate_callbacks: list = None
        self._sublayout_loader: callable = None
        self._bootstrap = bootstrap
        # _sublayout_loader must be set before call to _set_component_types()
        self._set_default_sublayout_loader()
        self._set_component_types()

        if 'post_validate_callbacks' in kwargs.keys():
            self.post_validate_callbacks = kwargs['post_validate_callbacks']
            if not isinstance(self.post_validate_callbacks, list):
                raise ValueError("post_validate_callbacks must be a list of functions")
        else:
            self.post_validate_callbacks = []

        self.convert_to_dash(**kwargs)

    def _set_component_types(self):
        _validate._validate_sublayout_loader_defined(self._sublayout_loader)
        self._datepickersingle_type = dcc.DatePickerSingle
        self._store_type = dcc.Store
        if self._bootstrap:
            self._label_type = dbc.Label
            self._button_type = dbc.Button
            self._checklist_type = dbc.Checklist
            self._input_type = dbc.Input
            self._dropdown_type = dbc.Select
        else:
            self._label_type = html.Label
            self._button_type = html.Button
            self._checklist_type = dcc.Checklist
            self._input_type = dcc.Input
            self._dropdown_type = dcc.Dropdown

        self._all_input_component_types = (
            self._datepickersingle_type,
            self._checklist_type,
            self._input_type,
            self._dropdown_type,
        )

    def _set_default_sublayout_loader(self):
        if self._bootstrap:
            self._sublayout_loader = _sublayouts.DashBootstrapSublayout()
        else:
            self._sublayout_loader = _sublayouts.DashCoreSublayout()

    def sublayout_loader(self, f: callable) -> callable:
        """Sets a callback for loading a sublayout,
        which is used to surround a DashForm input component with
        other desired dash components"""
        # TODO documentation
        self._sublayout_loader = f
        return f

    def _create_sublayout(self, component: Component, label: str) -> Sublayout:
        try:
            sublayout = self._sublayout_loader(component, label)
            _validate._validate_sublayout(sublayout, component)
            return sublayout
        except exceptions.SublayoutLoaderError as e:
            raise
        except TypeError as e:
            # Will catch any TypeError
            # TODO: How to modify to ensure it is only because of positional arguments?
            raise exceptions.SublayoutLoaderError(
                f"""
                The sublayout loader must accept 2 positional arguments: component and
                label. Original error:
                {e}
                """
            )

    def _add_component(
        self,
        component: Component,
        label: str=None,
        is_input: bool=False,
        id: str=''
    ) -> str:
        """Add a compontent to the Dash form

        :param component: the Dash component
        :type component: ComponentMeta or something
        :param is_input: if user can manipulate
        :type is_input: bool, optional
        :return: current id of component, if it exists, else None
        :rtype: str
        """
        if id:
            # TODO: May want to check if component already has id
            component.id = f'{self._prefix}{id}'
        if is_input:
            if component.id in self._inputs:
                raise exceptions.DuplicateFieldName(
                    f"""
                    There is already a field named {component.id}.
                    """)
            sublayout = self._create_sublayout(component, label)
            self._form.children.append(sublayout)
            self._inputs[component.id] = component
        else:
            self._form.children.append(component)
        return component.id if hasattr(component, 'id') else None

    def _process_field_kwargs(self, field: Field) -> Dict[str, Any]:
        """Process the keyword arguments for a field and adapt them
        to play nicely with Dash form components.

        :param field: the field to process
        :type field: Field (or UnboundField?)
        :return: keyword arguments for corresponding Dash component
        :rtype: dict
        """
        # TODO: clean up
        kwargs = {}
        if hasattr(field, 'id'):
            kwargs['id'] = field.id
        # TODO: Can this be called "placeholder" even if Field
        #       doesn't support that attribute?
        if hasattr(field, 'description') and field.description:
            kwargs['placeholder'] = field.description
        if hasattr(field, 'choices') and field.choices:
            if isinstance(field, SelectField):
                kwargs['options'] = []
                for choice in field.choices:
                    if not choice: break
                    # choice is pair of values, label is second
                    value, label = choice
                    option = {'label': label, 'value': value}
                    kwargs['options'].append(option)
        if hasattr(field, 'default') and field.default is not None:
            if isinstance(field, BooleanField):
                # TODO: Maybe find better way to represent a checklist value
                # that is, instead of '_'
                # currently limited to one checkbox, so could be anything
                kwargs['options'] = [{'label': '', 'value': '_'}]
                kwargs['value'] = ['_'] if field.default == True else []
            elif isinstance(field, (DateTimeField, DateField)):
                kwargs['date'] = field.default
            else:
                kwargs['value'] = field.default
        # NOTE: Maybe should check if field.label.text is a thing instead
        if hasattr(field, 'label'):
            if isinstance(field, BooleanField):
                # Merge existing options with a new label
                kwargs['options'] = [{**kwargs['options'][0], 'label': field.label.text}]
        kwargs['className'] = self._field_classname(field)
        return kwargs

    def _field_classname(self, field: Field) -> str:
        # TODO: some code reuse from _make_component. Do something about that?
        field_type = type(field)
        if (field_type is DecimalField
            or field_type is IntegerField
            or field_type is StringField
        ):
            return 'dashform-input'
        elif field_type is BooleanField:
            return 'dashform-checklist'
        elif (field_type is DateField
            or field_type is DateTimeField
        ):
            return 'dashform-date'
        elif field_type is SelectField:
            return 'dashform-dropdown'
        else:
            raise NotImplementedError(f"No handler for field type {field_type} (yet...)")

    def convert_to_dash(self,
                        button: Component=None,
                        store: Component=None,
                        **kwargs
    ):
        """Convert fields (from class definition) to Dash components in
        a layout

        :param button: Button for submit, defaults to html.Button("Submit", id="submit")
        :type button: html.Button, optional
        :param store: Store for output, defaults to dcc.Store(id="store", storage_type="memory")
        :type store: dcc.Store, optional
        :raises ValueError: If components passed have no id
        """

        # Warn if no fields declared, nothing to do
        if not self._fields:
            form_name = type(self).__name__
            warnings.warn(
                f"""
                No fields were defined for '{form_name}', form layout will be empty.
                """,
                exceptions.DashFormWarning
            )
            self._form = None
            return

        # TODO? a main div with two divs under it, one is for inputs and labels
        #   one is for button, store, error
        # all components in form (including labels, buttons, Store, etc)
        self._form = html.Div([])
        # dictionary of id to type for input components, useful for submit callback
        self._inputs = {}

        # get each Field, then add the Dash version to form
        for field_name in self._fields:
            field = self._fields[field_name]
            # TODO? Will always have label, but figure out if maybe there is
            # a case where we don't want one
            component, label = self._field_to_component(field)
            self._add_component(component, label, is_input=True)

        if button is None:
            # Add button and capture its id
            self._button_id = self._add_component(self._button_type("Submit"), id='submit')
        elif not hasattr(button, 'id'):
            raise ValueError("Button needs an id")
        else:
            # Add button that was passed in and capture its id
            self._button_id = self._add_component(button, id=button.id)

        if store is None:
            # Add store and capture its id
            # TODO: Fix _add_component in the case of a Store, redundant id passing
            store = self._store_type(storage_type="memory", id='store')
            self._store_id = self._add_component(store, id='store')
        elif not hasattr(store, 'id'):
            raise ValueError("Store needs an id")
        else:
            # Add store that was passed in and capture its id
            self._store_id = self._add_component(store, id=store.id)
        self._store = store

        # make divs for status output
        self._error_id = self._add_component(html.Div(), id='div_error')
        self._msg_id = self._add_component(html.Div(), id='div_msg')

    def _field_to_component(self, field: Field) -> Tuple[Component, str]:
        kwargs = self._process_field_kwargs(field)
        label = None
        if not isinstance(field, BooleanField) and hasattr(field, 'label'):
            label = field.label.text
        return self._make_component(field, **kwargs), label

    def _make_component(self, field: Field, **kwargs) -> Component:
        field_type = type(field)
        if (field_type is DecimalField
            or field_type is IntegerField
        ):
            return self._input_type(type='number', **kwargs)
        elif field_type is StringField:
            return self._input_type(type='text', **kwargs)
        elif field_type is BooleanField:
            return self._checklist_type(**kwargs)
        elif (field_type is DateField
            or field_type is DateTimeField
        ):
            return self._datepickersingle_type(**kwargs)
        elif field_type is SelectField:
            return self._dropdown_type(**kwargs)
        else:
            raise NotImplementedError(f"No handler for field type {field_type} (yet...)")

    def _generate_states(self):
        """Generate a list of States of all form fields

        NOTE to dev: if additonal states are needed, add them here
        """
        states = []
        # All states that are necessary for use in submit callback
        # includes all input components, plus the Store
        # use ordered dict in case order matters
        state_components = OrderedDict({**self._inputs, **{self._store_id: self._store}})

        for _, comp in state_components.items():
            # we don't care if component is a label
            # if (isinstance(comp, (html.Label, html.Br))
            #     or getattr(comp, 'id', None) == self._button_id
            # ):
            if (not isinstance(comp, self._all_input_component_types)
                and not isinstance(comp, self._store_type)
            ):
                continue
            try:
                states.extend(self._get_component_state(comp))
            except AttributeError as e:
                raise AttributeError(f"Couldn't add state because of attribute error: {e}")
            except Exception as e:
                raise Exception(f"Something else happened: {e}")
        return states

    def _get_component_state(self, comp: Component) -> List:
        states = []
        states.append(State(comp.id, 'id'))
        if isinstance(comp, (
            self._input_type, self._checklist_type, self._dropdown_type)
        ):
            states.append(State(comp.id, 'value'))
        elif isinstance(comp, self._datepickersingle_type):
            states.append(State(comp.id, 'date'))
        elif comp.id == self._store_id:
            states.append(State(comp.id, 'data'))
        return states

    def _get_property_name(self, input_id: str) -> Tuple[str, ...]:
        """Given the id of the Dash form input component, return
        the names of the attributes we care about (the ones for
        validating form input)

        :param input_id: ID of input component
        :type input_id: str
        :return: tuple of property names of component
        :rtype: Tuple[str, ...]
        """
        comp = self._inputs[input_id]
        if isinstance(comp,
            (self._input_type, self._checklist_type, self._dropdown_type)
        ):
            return ('value',)
        elif isinstance(comp, self._datepickersingle_type):
            return ('date',)
        # TODO for if/when datepickerrange can be used
        # elif input_type == dcc.DatePickerRange:
        #     return ('start_date', 'end_date',)
        else:
            return None

    def _get_property_value(self, input_id: str, value: Any) -> Any:
        """Get the underlying value of the Dash form input component

        :param input_id: ID of input component
        :type input_id: str
        :param value: [description]
        :type value: Any
        :return: value of component, compatible with corresponding Field
        :rtype: Any
        """
        comp = self._inputs[input_id]
        if isinstance(comp, (self._input_type, self._datepickersingle_type)):
            return value
        if isinstance(comp, self._checklist_type):
            return '_' in value
        if isinstance(comp, self._dropdown_type):
            return value
        else:
            return None

    def on_success_post_submit(self, *args, **kwargs) -> Dict:
        """Called when form submitted and validation is successful

        Store data is passed in through kwargs,
        edited/augmented as necessary and returned

        :return: The Store data to return; by default, the original
                 data passed in
        :rtype: Dict
        XXX: For a child class that overrides -- Use caution
        Currently unsure if kwargs should a) be replaced by parent's own
        on_success_post_submit() value, or b) be merged with it
        """
        return kwargs

    def post_validate(self) -> bool:
        # TODO think about how to get external errors to prevent form post validation
        # Basically if any callbacks return False, this function returns False
        return all(filter(lambda x: not x, self.post_validate_callbacks))

    def register_callbacks(self,
            app: dash.Dash,
            submit_decorator: callable=None
        ):
        '''Register callbacks for the Dash form

        TODO: Is there a way to show a dcc.Loading until everything is
              initialized? Some callbacks need time to register, especially
              from derived classes.

        Parameters
        ----------
        app : dash.Dash
            Dash app that contains the Dash form
        submit_decorator : callable, optional
            NOTE: Probably a temporary workaround
            Additional decorator to apply to one or more callbacks

            The original idea being to "undo" any wrappers or decoractors
            applied to the Dash.callback() decorator, like additional
            arguments added (for database access, etc)

            by default None
            TODO: Possibly figure out better way to "undo" decorators or
                preserve original expected positional arguments for below
                callbacks
                Or better yet, it should probably be handled outside this
                class.

        Raises
        ------
        TypeError
            [description]
        PreventUpdate
            [description]
        PreventUpdate
            [description]
        '''

        # https://www.saltycrane.com/blog/2010/03/simple-python-decorator-examples/
        # Using identity decorator
        def do_nothing_decorator(func: callable) -> callable:
            return func

        if submit_decorator is None:
            submit_decorator = do_nothing_decorator
        if not callable(submit_decorator):
            raise TypeError(f"{submit_decorator.__name__} must be a decorator")

        @app.callback(
            [Output(self._store_id, 'data'),
             Output(self._error_id, 'children'),
             Output(self._msg_id, 'children')],
            [Input(self._button_id, 'n_clicks')],
            self._generate_states()
        )
        @submit_decorator
        def submit(n_clicks: int, *states: State) -> Tuple[dict, str, str]:
            """Callback for populating dcc.Store upon success

            Also for returning error or success messages

            Returns
            -------
            data : dict
                [description]
            error : str
                [description]
            msg : str
                [description]
            """
            # No update if no clicks
            if n_clicks is None:
                raise PreventUpdate
            try:
                # TODO: states of component id not necessary, remove in future
                idx_store = states.index(self._store_id)
                # Get current state of store.data
                data = states[idx_store+1]
                # if nothing return empty dict
                data = data or {}
                # If successful, 'validated' key is positive (truthy)
                # If not successful, it is 0 (falsy)
                if not 'validated' in data.keys():
                    data['validated'] = 0
                error = ''
                msg = ''

                # iterate through all input ids
                # (not including Store or Button)
                for input_id in self._inputs:
                    # get corresponding field name for validation purposes
                    # which basically just means remove the prefix
                    field_name = input_id.replace(self._prefix, '')
                    field = self._fields[field_name]
                    # throw exception if not found
                    idx = states.index(input_id)
                    # populate underlying WTForm Fields
                    # TODO: Support more than one value
                    field.data = states[idx+1]
                # Validate form inputs
                if self.validate() and self.post_validate():
                    # If success, populate Store
                    for input_id in self._inputs:
                        idx = states.index(input_id)
                        for prop in self._get_property_name(input_id):
                            prop_val = self._get_property_value(input_id, states[idx+1])
                            data[f'{input_id}.{prop}'] = prop_val
                    # Meant to force update modified_timestamp for use in callbacks
                    data['validated'] += 1
                    # TODO Allow custom messages
                    msg = "Success"
                else:
                    # If failure, show error
                    # get dict of errors (field: error)
                    all_errors = self.errors
                    errs: List[str] = []
                    for f in all_errors.keys():
                        for e in all_errors[f]:
                            # Create a detailed error message
                            err = f'{self._fields[f].label.text.replace(": ", "")}: {e}'
                            errs.append(err)
                    error = ', '.join(errs)
                    data['validated'] = 0
            except:
                import traceback
                traceback.print_exc()
            finally:
                return (self.on_success_post_submit(**data) if not error else data,
                    error,
                    msg)

    @property
    def layout(self) -> Sublayout:
        """return Dash layout of form

        :return: Dash layout
        :rtype: html.Div (or other Component type?)
        """
        return self._form

    @property
    def data(self) -> Dict[str, Any]:
        """return data from Store (populated if form validation successful)

        :return: data from Store
        :rtype: dict
        """
        return getattr(self._form[self._store_id], 'data', None)

    @property
    def store_id(self) -> str:
        """ID of Store
        """
        return self._store_id

    @property
    def button_id(self) -> str:
        """ID of Button
        """
        return self._button_id

class DashForm(BaseDashForm):
    """DashForm class used to create validation-enabled forms in Dash
    using WTF-style definitions.

    Parameters
    ----------
    BaseDashForm :
        [description]
    """

    DEFAULT_TO_BOOTSTRAP = False

    def __init__(self, *args, **kwargs):
        # if bootstrap explicitly set, override DEFAULT_TO_BOOTSTRAP
        kwargs = {
            **{'bootstrap': self.DEFAULT_TO_BOOTSTRAP},
            **kwargs,
        }
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        warnings.warn(
            """
            DashForm is not meant to be instantiated directly.
            Instead, define a new class that derives from DashForm.
            """,
            exceptions.DashFormWarning
        )
        # Empty layout
        self._form = None
        # No callbacks
        self.register_callbacks = lambda d: ...
