from textwrap import dedent


class DashFormException(Exception):
    def __init__(self, msg=""):
        super().__init__(dedent(msg).strip())


class DashFormWarning(UserWarning):
    def __init__(self, msg=""):
        super().__init__("DashForm: " + dedent(msg).strip())


class ComponentNotInSublayoutError(DashFormException):
    pass


class SublayoutLoaderError(DashFormException):
    pass


# base exception for all invalid field names
class InvalidFieldName(DashFormException):
    pass


class DuplicateFieldName(InvalidFieldName):
    pass
