"""
Sample form made with Dash-WTF
"""
import dash
import datetime as dt
import dash_html_components as html

from dash.dependencies import Output, Input
from dash.exceptions import PreventUpdate
from wtforms import (
    StringField,
    IntegerField,
    BooleanField,
    DateTimeField,
    DecimalField,
    SelectField
)
from wtforms import validators, ValidationError

from dash_wtf import DashForm

class SampleForm(DashForm):
    """Define your fields with WTForms field declarations"""
    username = StringField("Username", description="Enter...", default='BuddyBoy',
        validators=[validators.Length(
            min=8,
            message="Username must be at least 8 characters long"
        )]
    )
    length = IntegerField("Length", description="Enter...", default=8,
        validators=[validators.NumberRange(min=8, max=32,
            message="Enter a number between 8 and 32"
        )]
    )
    public = BooleanField("This is a public computer", default=False)
    birth = DateTimeField("Date of Birth", default=dt.date(1993, 12, 11))
    weight = DecimalField("Your Weight (kg)", default=62.0)
    favorite_color = SelectField("Favorite Color", default='r',
        choices=[('r', "Red"), ('y', "Yellow"), ('b', "Blue"), ('g', "Green"), ('p', "Purple")])

    def validate_username(form, field):
        """Username must have exactly as many characters
        as specified in the length field"""
        if (field.data is not None
                and len(field.data) != form['length'].data):
            raise ValidationError(
                f'Username must be {form["length"].data} characters long'
            )


def main():
    """My main function"""
    form = SampleForm(prefix="sample-form-")
    app = dash.Dash(__name__)
    app.layout = html.Div([
        form.layout,
        html.Div(id='zodiac')
    ])
    form.register_callbacks(app)

    # Add a callback that extracts data from the form
    # This will give the Chinese zodiac based on year of birth
    @app.callback(
        Output('zodiac', 'children'),
        [
            Input(form._store_id, 'data')
        ]
    )
    def chinese_zodiac(data):
        if not 'validated' in data:
            raise PreventUpdate
        signs = [
            'Monkey',
            'Rooster',
            'Dog',
            'Pig',
            'Rat',
            'Ox',
            'Tiger',
            'Rabbit',
            'Dragon',
            'Snake',
            'Horse',
            'Sheep',
        ]
        date = data[f'{form.birth.id}.date']
        date = dt.datetime.strptime(date, '%Y-%m-%d')
        year = date.year
        return f"You were born in the Year of the {signs[year % 12]}"

    app.run_server(debug=True, host='0.0.0.0')


if __name__ == '__main__':
    main()
