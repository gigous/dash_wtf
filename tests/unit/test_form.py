"""Tests dash form
NOTE: Tested below as of 2020/12/3
"""

from dash_wtf.dashform import DashForm
from dash_wtf.exceptions import DashFormWarning
import time
import pytest
import dash_html_components as html
import dash_core_components as dcc
from dash.exceptions import PreventUpdate
from wtforms import StringField, IntegerField
from selenium.webdriver.common.keys import Keys
import pytest

# pylint: disable=protected-access
# TODO need way of testing bootstrap AND core components

def create_form(
        formdata=None,
        obj=None,
        prefix="",
        data=None,
        meta=None,
        bootstrap=False,
        **kwargs
    ):
    from dash_wtf import DashForm
    class TestForm(DashForm):
        @classmethod
        def populate_form(cls, **kwargs):
            """creates fields within form class definition
            just like you normally would, but using keyword
            arguments instead
            """
            for name in kwargs:
                setattr(cls, name, kwargs[name])
    post_validate_callbacks = []
    if 'post_validate_callbacks' in kwargs:
        post_validate_callbacks = kwargs.pop('post_validate_callbacks')
    TestForm.populate_form(**kwargs)
    return TestForm(formdata=formdata, obj=obj, prefix=prefix, data=data, meta=meta,
        bootstrap=bootstrap, post_validate_callbacks=post_validate_callbacks)


@pytest.fixture
def dash_app():
    """Make a Dash app and spit it out like a cheesecake factory"""
    import dash
    def _create_dash_app():
        app = dash.Dash('pytest_dash')
        return app
    return _create_dash_app


@pytest.fixture
def make_and_apply_dash_form():
    """Make a form to do tests on and apply it to Dash app"""
    def _create_form(app, prefix="", **kwargs):
        dash_form = create_form(prefix=prefix, **kwargs)
        app.layout = dash_form.layout
        return dash_form

    return _create_form


class TestDashForm():
    def _all_ids_have_match(self, dash_form):
        fields = dash_form._fields
        field_ids = [fields[name].id for name in fields]
        comp_ids = dash_form._inputs
        # ! assuming no duplicate ids (which should not happen anyway)
        return set(comp_ids) == set(field_ids)

    def _is_state(self, states, state_name):
        for state in states:
            if state_name == state.component_id:
                return True
        return False

    def _state_has_property(self, states, state_name, state_prop):
        for state in states:
            if state_name == state.component_id and state_prop == state.component_property:
                return True
        return False

    #######################
    #     TESTS BEGIN     #
    #######################

    # NOTE: Can do individual tests with pytest -k {tcid}
    # Ex for below test: pytest -k tdf001
    def test_tdf001_uno(self):
        assert True

    def test_tdf002_dashform_creation_(self):
        class TestForm(DashForm):
            username = StringField("Username1")
            pass

        dash_form = TestForm()
        assert dash_form

    # Test function to test create form function
    def test_tdf003_create_form_func(self):
        dash_form = create_form(username=StringField("Username1"))
        assert 'username' in dash_form._inputs

    @pytest.mark.filterwarnings('ignore')
    def test_tdf004_form_empty_layout(self):
        assert create_form().layout == None

    def test_tdf005_form_create_field(self):
        dash_form = create_form(prefix="form", username=StringField())
        assert dash_form.layout is not None
        assert isinstance(dash_form.layout, html.Div)
        assert len(dash_form.layout.children) > 0
        assert self._all_ids_have_match(dash_form)

    def test_tdf006_form_generate_states_standard(self):
        dash_form = create_form(username=StringField(), points=IntegerField())
        states = dash_form._generate_states()
        # TODO: states of component id not necessary, remove in future
        assert self._is_state(states, 'username')
        assert self._is_state(states, 'points')
        assert self._is_state(states, dash_form._store_id)
        assert self._state_has_property(states, 'username', 'value')
        assert self._state_has_property(states, 'points', 'value')
        assert self._state_has_property(states, dash_form._store_id, 'data')

    def test_tdf007_form_custom_button(self):
        dash_form = create_form(username=StringField(), points=IntegerField())
        # would have called convert_to_dash() already but that's okay
        btn = html.Button("GO", id="btn_go")
        dash_form.convert_to_dash(button=btn)
        assert btn in dash_form._form.children

    @pytest.mark.filterwarnings('ignore')
    def test_tdf008_dashform_creation_no_fields(self):
        from dash_wtf import DashForm
        class TestForm(DashForm):
            pass

        dash_form = TestForm()
        assert dash_form

    @pytest.mark.filterwarnings('error')
    def test_tdf009_no_fields_raises_warning(self):
        from dash_wtf import DashForm
        class TestForm(DashForm):
            pass

        with pytest.raises(DashFormWarning):
            assert TestForm()

    @pytest.mark.filterwarnings('error')
    def test_tdf010_dashform_call_raises_warning(self):
        from dash_wtf import DashForm
        with pytest.raises(DashFormWarning):
            assert DashForm()

    def test_tdf011_dash_bootstrap_not_installed_exception(self):
        with pytest.raises(Exception):
            assert create_form(bootstrap=True)


@pytest.mark.usefixtures('dash_duo', 'dash_app')
class TestDashFormPage():
    #def test_webdriver(self, dash_duo):
    #    pass

    def _simple_form(self):
        return {
            'username': StringField(default="hello"),
            'points': IntegerField(default=33)
        }

    def test_tdfp001_page_contains_comps(self, dash_duo, dash_app, make_and_apply_dash_form):
        app = dash_app()
        prefix = "form_"
        dash_form = make_and_apply_dash_form(app, prefix=prefix, **self._simple_form())

        dash_duo.start_server(app)
        time.sleep(3)
        assert dash_duo.find_element(f"#{prefix}username")
        assert dash_duo.find_element(f"#{prefix}points")
        assert dash_duo.find_element(f'#{prefix}submit')
        print(app.index())


    def test_tdfp002_register_callbacks(self, dash_duo, dash_app, make_and_apply_dash_form):
        app = dash_app()
        prefix = "form_"
        dash_form = make_and_apply_dash_form(app, prefix=prefix, **self._simple_form())
        try:
            dash_form.register_callbacks(app)
        except:
            assert 0

        dash_duo.start_server(app)
        assert dash_duo.find_element(f"#{prefix}username")
        assert dash_duo.find_element(f"#{prefix}points")
        assert dash_duo.find_element(f'#{prefix}submit')


    def _web_element_clear(self, we):
        """ because clear() does nothing, apparently... """
        we.send_keys(Keys.CONTROL + "a")
        we.send_keys(Keys.DELETE)


    def test_tdfp003_points_is_22(self, dash_duo, dash_app, make_and_apply_dash_form):
        app = dash_app()
        prefix = "form_"
        dash_form = make_and_apply_dash_form(app, prefix=prefix, **self._simple_form())
        dash_form.register_callbacks(app)

        dash_duo.start_server(app)
        points = dash_duo.find_element(f'#{prefix}points')

        assert points.get_attribute('value') == '33'
        self._web_element_clear(points)
        assert dash_duo.wait_for_text_to_equal(f'#{prefix}points', "", timeout=3)
        points.send_keys("22")
        assert dash_duo.wait_for_text_to_equal(f'#{prefix}points', "22", timeout=3)


    def test_tdfp004_username_is_hello_world(self, dash_duo, dash_app, make_and_apply_dash_form):
        app = dash_app()
        prefix = "form_"
        dash_form = make_and_apply_dash_form(app, prefix=prefix, **self._simple_form())
        dash_form.register_callbacks(app)

        dash_duo.start_server(app)
        username = dash_duo.find_element(f'#{prefix}username')

        assert username.get_attribute('value') == 'hello'
        username.send_keys(" world")
        assert dash_duo.wait_for_text_to_equal(f"#{prefix}username", "hello world", timeout=3)


    def test_tdfp005_values_in_store(self, dash_duo, dash_app, make_and_apply_dash_form):
        # Not sure how to check if data in store
        # return instead
        return
        app = dash_app()
        prefix = "form_"
        dash_form = make_and_apply_dash_form(app, prefix=prefix, **self._simple_form())
        #dash_form.register_callbacks(app)

        from dash.dependencies import Output, Input, State
        @app.callback(
            Output(dash_form._button_id, 'children'),
            [Input(dash_form.store_id, 'data'),
            ]
        )
        def callback_function_name(input_property):
            if input_property is None:
                raise PreventUpdate()
            assert input_property == {
                'username.value': 'hello',
                'points.value': 33
            }

        store_id = dash_form.store_id

        dash_duo.start_server(app)
        #print(app.layout)
        assert dash_duo.find_element(f"#{prefix}username")
        assert dash_duo.find_element(f"#{prefix}points")
        submit = dash_duo.find_element(f'#{prefix}submit')
        assert submit

        submit.click()

        assert app.layout[store_id] is dash_form._store
        assert dash_form._store is dash_form.layout[store_id]
        assert dash_form._store in dash_form.layout.children
        assert isinstance(dash_form._store, dcc.Store)
        time.sleep(3)
        assert 'data' in dash_form._store.available_properties


# pylint: enable=protected-access
